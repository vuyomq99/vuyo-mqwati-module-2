import 'dart:math';

void main() {
  var cowa = new App();
  cowa.Application = "cowa-bunga";
  cowa.Category = "Best Enterprise Solution";
  cowa.Developer = "Kimberley Taylor and Bradley Rimmer";
  cowa.year = 2018;

  var acgl = new App();
  acgl.Application = "acgl";
  acgl.Category = "Best Gaming Solution";
  acgl.Developer = "Peter Appel";
  acgl.year = 2018;

  var besmarta = new App();
  besmarta.Application = "besmarta";
  besmarta.Category = "Best Incubated Solution";
  besmarta.Developer = "Kobus Louw";
  besmarta.year = 2018;

  var Xander = new App();
  Xander.Application = "Xander English";
  Xander.Category = "Best education Solution";
  Xander.Developer = "Sibella Knot-Craig";
  Xander.year = 2018;

  var ctrl = new App();
  ctrl.Application = "CTRL";
  ctrl.Category = "Best financial Solution";
  ctrl.Developer = "Pieter Erasmus, Pieter Venter and Francois Venter";
  ctrl.year = 2018;

  var pine = new App();
  pine.Application = "Pineapple";
  pine.Category = "Best Consumer Solution";
  pine.Developer =
      "Mathew Smith, Marnus van Heerden, Ndabenhle Nglube and Sizwe Ndlovu";
  pine.year = 2018;

  var bestee = new App();
  bestee.Application = "Bestee";
  bestee.Category = "Most innovative solution";
  bestee.Developer = "Emile Ferrreira";
  bestee.year = 2018;

  var stock = new App();
  stock.Application = "Stokfella";
  stock.Category = "Best South African Solution";
  stock.Developer = "Tshepo Moloi";
  stock.year = 2018;

  var asi = new App();
  asi.Application = "asi snakes";
  asi.Category = "Peoples Choice Award";
  asi.Developer = "Johan Marais";
  asi.year = 2018;

  var trans = new App();
  trans.Application = "Transunion-1check";
  trans.Category = "Best Enterprise Solution";
  trans.Developer = "N.A";
  trans.year = 2017;

  var orderin = new App();
  orderin.Application = "OrderIn";
  orderin.Category = "Best consumer solution";
  orderin.Developer = "Dinesh Patel";
  orderin.year = 2017;

  var ecoslips = new App();
  ecoslips.Application = "EcoSlips";
  ecoslips.Category = "Best incubated solution";
  ecoslips.Developer = "Henco Schoeman";
  ecoslips.year = 2017;

  var intergreatme = new App();
  intergreatme.Application = "InterGreatMe";
  intergreatme.Category = "Most innovative solution";
  intergreatme.Developer = "Dewal Thiart";
  intergreatme.year = 2017;

  var zulzi = new App();
  zulzi.Application = "Zulzi";
  zulzi.Category = "Best Breakthrough Developer";
  zulzi.Developer = "Donald Valoyi";
  zulzi.year = 2017;

  var jude = new App();
  jude.Application = "Hey Jude";
  jude.Category = "Best South African App";
  jude.Developer = "Marcus Smith";
  jude.year = 2017;

  var oru = new App();
  oru.Application = "The ORU Social";
  oru.Category = "Women in STEM";
  oru.Developer = "Nokuzola Thetsane";
  oru.year = 2017;

  var superanimals = new App();
  superanimals.Application = "Super Animals 2";
  superanimals.Category = "Best gaming solution";
  superanimals.Developer = "Sea MOnster";
  superanimals.year = 2017;

  var treeapp = new App();
  treeapp.Application = "The TreeApp South Africa";
  treeapp.Category = "Best Agriculture Solution";
  treeapp.Developer = "Jules Buker";
  treeapp.year = 2017;

  var health = new App();
  health.Application = "WatIf Health Portal";
  health.Category = "Best Health Solution";
  health.Developer = "Norman Sipula";
  health.year = 2017;

  var awethu = new App();
  awethu.Application = "awethu Project";
  awethu.Category = "Best Education Solution";
  awethu.Developer = "Yusuf Randera-Rees";
  awethu.year = 2017;

  var shyft = new App();
  shyft.Application = "shyft for Standard Bank";
  shyft.Category = "Best Financial Solution";
  shyft.Developer = "Brett Patrontasch";
  shyft.year = 2017;

  cowa.printApplicationInformation();
  acgl.printApplicationInformation();
  besmarta.printApplicationInformation();
  Xander.printApplicationInformation();
  ctrl.printApplicationInformation();
  pine.printApplicationInformation();
  bestee.printApplicationInformation();
  stock.printApplicationInformation();
  asi.printApplicationInformation();
  trans.printApplicationInformation();
  orderin.printApplicationInformation();
  ecoslips.printApplicationInformation();
  intergreatme.printApplicationInformation();
  zulzi.printApplicationInformation();
  jude.printApplicationInformation();
  oru.printApplicationInformation();
  superanimals.printApplicationInformation();
  treeapp.printApplicationInformation();
  health.printApplicationInformation();
  awethu.printApplicationInformation();
  shyft.printApplicationInformation();
}

class App {
  String? Application;
  String? Category;
  String? Developer;
  int? year;

  void printApplicationInformation() {
    print("New entry");
    print("Name of application is $Application".toUpperCase());
    print("Category of app is $Category");
    print("Name of developer/s is/are $Developer");
    print("Year the app won is $year");
  }
}
